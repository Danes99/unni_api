import { PrismaClient } from '@prisma/client'
import { ingredients } from '../data/ingredients'
const prisma = new PrismaClient()

const name = "clement"
const email = "clement.stauner@gmail.com"
const password = "pass12345"

async function main() {

  const user = await prisma.user.create({
    data: {
      email,
      name,
      password,
      role: 'ADMIN',
    },
  })

  const userId = user.id

  await prisma.ingredient.createMany({
    data: ingredients.map(e => ({ userId, ...e })),
    // data: ingredients,
  })
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
