// Import downloaded modules
// const { PrismaClient } = require("@prisma/client")
// const { PrismaClient: prismaClientDB } = require('../prisma/generated/prisma-client-js')

const { PrismaClient: PrismaClientID } = require('../prisma/id/prisma-client-js')
const { PrismaClient: PrismaClientXks } = require('../prisma/xks/prisma-client-js')
const { PrismaClient: PrismaClientRestaurant } = require('../prisma/restaurant/prisma-client-js')

// Import custom functions
const { hashPassword } = require('../utils/hash')

function generateNewPrismaClient() {

  const client = new prismaClientDB()

  // Hash user password before save
  client.$use(async (params, next) => {

    if (params.model === 'User' && (params.action === 'create' || params.action === 'update')) {

      // If password is undefined
      // Thats means the user does not want to update it
      // So, we don't generate an hash of 'undefined'
      if (params.args.data.password) {

        params.args.data.password = await hashPassword(
          params.args.data.password
        )

      }
    }

    return await next(params)
  })

  return client
}

function generateNewPrismaClientID() {

  const client = new PrismaClientID()

  // Hash user password before save
  client.$use(async (params, next) => {

    if (params.model === 'User' && (params.action === 'create' || params.action === 'update')) {

      // If password is undefined
      // Thats means the user does not want to update it
      // So, we don't generate an hash of 'undefined'
      if (params.args.data.password) {

        params.args.data.password = await hashPassword(
          params.args.data.password
        )

      }
    }

    return await next(params)
  })

  return client
}

// export const prisma = global.prisma || new PrismaClient()
// const prisma = global.prisma || generateNewPrismaClient()
// exports.prisma = prisma

// Prisma Client: ID DB 
const prismaClientID = global.prismaClientID || generateNewPrismaClientID()
exports.prismaClientID = prismaClientID

// Prisma Client: Restaurant DB 
const prismaClientRestaurant = global.prismaClientRestaurant || new PrismaClientRestaurant()
exports.prismaClientRestaurant = prismaClientRestaurant

// Prisma Client: XKS DB
const prismaClientXks = global.prismaClientXks || new PrismaClientXks()
exports.prismaClientXks = prismaClientXks

if (process.env.NODE_ENV === "development") {
  // global.prisma = prisma
  global.prismaClientID = prismaClientID
  global.prismaClientXks = prismaClientXks
  global.prismaClientRestaurant = prismaClientRestaurant
}
