// Import build-in modules
const { join } = require("path")

// Import downloaded modules
const {
  makeSchema,
  // Nexus.js Plugins
  fieldAuthorizePlugin
} = require("nexus")

// Import Nexus.js GraphQL type definitions
const types = require("./types")

const schema = makeSchema({
  types,
  outputs: {
    // typegen: join(
    //   process.cwd(),
    //   "node_modules", "@types", "nexus-typegen", "index.d.ts"
    // ),
    schema: join(__dirname, "schema.graphql"),
  },
  contextType: {
    export: "Context",
    module: join(__dirname, "context.js"),
  },
  plugins: [
    // ... other plugins
    fieldAuthorizePlugin(),
  ],
})

module.exports = schema
