const {
  nonNull,
  stringArg,
  extendType,
  objectType
} = require('nexus')

const UserEvent = objectType({
  name: 'UserEvent',
  definition(t) {
    // Meta data
    t.string('id')
    t.string('createdAt')
    t.string('generatedAt')
    // Data
    t.string('userId')
    t.string('type')
    t.string('htmlId')
    t.string('href')
  },
})

exports.MutationCreateUserEvent = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('createUserEvent', {
      type: UserEvent,
      args: {
        generatedAt: nonNull(stringArg()),
        href: nonNull(stringArg()),
        type: nonNull(stringArg()),
        htmlId: nonNull(stringArg()),
        href: nonNull(stringArg()),
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, args, context) {

        return await context.prismaClientXks.event.create({
          data: {
            userId: context.user.id,
            ...args
          }
        })
      },
    })
  },
})