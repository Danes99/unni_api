// Import everything
// And export in one single file

// Restaurant
exports.ingredient = require("./ingredient")
exports.recipe = require("./recipe")
exports.recipeIngredient = require("./recipeIngredient")
exports.tag = require('./tag')
exports.order = require("./order")

// User
exports.user = require("./user")

// Data Collection
exports.dataCollection = require("./dataCollection")
