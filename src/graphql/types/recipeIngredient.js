const {
  floatArg,
  nonNull,
  objectType,
  stringArg,
  extendType,
} = require('nexus')

const RecipeIngredient = objectType({
  name: 'RecipeIngredient',
  definition(t) {
    t.string('id')
    t.string('createdAt')
    t.string('updatedAt')

    t.string('recipeId')
    t.string('ingredientId')
    t.float('quantity')

    t.field('details', {
      type: 'Ingredient',
      async resolve(_parent, _args, context) {
        try {
          return await context.prismaClientRestaurant.ingredient
            .findUnique({
              where: {
                id: _parent.ingredientId,
              },
            })
        } catch (error) {
          console.error(error)
        }
      },
    })

  },
})

// Exports
exports.RecipeIngredient = RecipeIngredient

// Read Unique Recipe Ingredient
exports.QueryRecipeIngredientByID = extendType({
  type: 'Query',
  definition(t) {
    t.nonNull.field('RecipeIngredient', {
      type: RecipeIngredient,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, args, context) {

        return await context.prismaClientRestaurant.recipeIngredient.findFirst({
          where: {
            id: args.id,
            userId: context.user.id
          },
        })

      },
    })
  },
})

// Create Recipe Ingredient
exports.MutationCreateRecipeIngredient = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('createRecipeIngredient', {
      type: RecipeIngredient,
      args: {
        recipeId: nonNull(stringArg()),
        ingredientId: nonNull(stringArg()),
        quantity: nonNull(floatArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, args, context) {

        return await context.prismaClientRestaurant.recipeIngredient.create({
          data: { ...args },
        })

      },
    })
  },
})

// Update Recipe Ingredient
exports.MutationUpdateRecipeIngredient = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('updateRecipeIngredient', {
      type: RecipeIngredient,
      args: {
        id: nonNull(stringArg()),
        title: stringArg(),
        description: stringArg(),
        price: floatArg()
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id, ...data }, context) {

        return await context.prismaClientRestaurant.recipeIngredient.update({
          where: { id },
          data,
        })

      },
    })
  },
})

// Delete Recipe Ingredient
exports.MutationDeleteRecipeIngredient = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('deleteRecipeIngredient', {
      type: RecipeIngredient,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id }, context) {

        return await context.prismaClientRestaurant.recipeIngredient.delete({
          where: { id }
        })

      },
    })
  },
})