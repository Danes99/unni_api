const {
  floatArg,
  nonNull,
  objectType,
  stringArg,
  extendType,
  enumType,
  inputObjectType,
  list,
  // enumType,
} = require('nexus')

const Recipe = objectType({
  name: 'Recipe',
  definition(t) {
    t.nonNull.string('id')
    t.nonNull.string('createdAt')
    t.nonNull.string('updatedAt')
    t.nonNull.string('userId')

    t.nonNull.string('title')
    t.nonNull.string('description')
    t.nonNull.float('price')
    t.field('category', { type: RecipeCategory })

    t.list.field('ingredients', {
      type: 'RecipeIngredient',
      async resolve(_parent, _args, context) {
        return await context.prismaClientRestaurant.recipe
          .findUnique({
            where: {
              id: _parent.id,
            },
          }).ingredients()
      },
    })

    t.list.field('tags', {
      type: 'Tag',
      async resolve(_parent, _args, context) {

        const RecipeTags = await context.prismaClientRestaurant.recipe
          .findUnique({
            where: {
              id: _parent.id,
            },
          })
          .tags()

        return await Promise.all(RecipeTags.map(element =>
          context.prismaClientRestaurant.tag.findFirst({
            where: {
              id: element.tagId
            },
          })
        ))
      },
    })

    t.list.field('orders', {
      type: 'Order',
      async resolve(_parent, _args, context) {
        return await context.prismaClientRestaurant.recipe.findUnique({
          where: {
            id: _parent.id,
          },
        }).orders()
      },
    })

  },
})

const RecipeCategory = enumType({
  name: 'Type',
  members: ['DISH', 'DRINK', 'DISH_SINGULAR', 'DRINK_SINGULAR'],
})

const RecipeIngredientInputType = inputObjectType({
  name: 'RecipeIngredientInputType',
  definition(t) {
    t.nonNull.string('ingredientId')
    t.nonNull.float('quantity')
  }
})

const UpdateRecipeIngredientInputType = inputObjectType({
  name: 'UpdateRecipeIngredientInputType',
  definition(t) {
    t.nonNull.string('id')
    t.string('ingredientId')
    t.float('quantity')
  }
})

// Exports
exports.Recipe = Recipe
exports.RecipeIngredientInputType = RecipeIngredientInputType
exports.UpdateRecipeIngredientInputType = UpdateRecipeIngredientInputType

// Read Unique Recipe
exports.QueryRecipeByID = extendType({
  type: 'Query',
  definition(t) {
    t.nonNull.field('Recipe', {
      type: Recipe,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id }, context) {

        return await context.prismaClientRestaurant.recipe.findFirst({
          where: {
            id,
            userId: context.user.id
          },
        })

      },
    })
  },
})

// Create Recipe
exports.MutationCreateRecipe = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('createRecipe', {
      type: Recipe,
      args: {
        title: nonNull(stringArg()),
        description: nonNull(stringArg()),
        price: nonNull(floatArg()),
        category: stringArg(),
        // category: RecipeCategory,
        ingredients: list(RecipeIngredientInputType),
        tags: list(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { ingredients, tags, ...recipeArgs }, context) {

        const recipe = await context.prismaClientRestaurant.recipe.create({
          data: {
            ...recipeArgs,
            userId: context.user.id
          }
        })

        if (ingredients?.length > 0) {

          const result = await Promise.all(
            ingredients.map(element =>
              context.prismaClientRestaurant.recipeIngredient.create({
                data: {
                  recipeId: recipe.id,
                  ingredientId: element.ingredientId,
                  quantity: element.quantity
                }
              })
            )
          )
          recipe.ingredient = result
        }

        if (tags?.length > 0) {

          const result = await Promise.all(
            tags.map(element =>
              context.prismaClientRestaurant.recipeTag.create({
                data: {
                  recipeId: recipe.id,
                  tagId: element
                }
              })
            )
          )
          recipe.tags = result
        }

        return recipe
      },
    })
  },
})

// Update Recipe
exports.MutationUpdateRecipe = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('updateRecipe', {
      type: Recipe,
      args: {
        id: nonNull(stringArg()),
        title: stringArg(),
        description: stringArg(),
        price: floatArg(),
        category: stringArg(),
        // category: nonNull(RecipeCategory),
        ingredients: list(UpdateRecipeIngredientInputType)
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id, ingredients, ...data }, context) {

        await Promise.all(
          ingredients.map(
            element =>
              context.prismaClientRestaurant.recipeIngredient.update({
                where: {
                  id: element.id
                },
                data: {
                  ingredientId: element.ingredientId,
                  quantity: element.quantity
                }
              })
          )
        )

        return await context.prismaClientRestaurant.recipe.update({
          where: { id },
          data,
        })
      },
    })
  },
})

// Delete Recipe
exports.MutationDeleteRecipe = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('deleteRecipe', {
      type: Recipe,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id }, context) {

        return await context.prismaClientRestaurant.recipe.delete({
          where: { id }
        })

      },
    })
  },
})