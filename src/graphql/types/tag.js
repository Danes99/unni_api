const {
  nonNull,
  objectType,
  stringArg,
  extendType
} = require('nexus')

const Tag = objectType({
  name: 'Tag',
  definition(t) {
    t.string('id')
    t.string('createdAt')
    t.string('updatedAt')

    t.string('title')
    t.string('color')
  },
})

// Exports
exports.Tag = Tag

// Read Unique Tag
exports.QueryTagByID = extendType({
  type: 'Query',
  definition(t) {
    t.nonNull.field('Tag', {
      type: Tag,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id }, context) {

        return await context.prismaClientRestaurant.tag.findFirst({
          where: {
            id,
            userId: context.user.id
          },
        })

      },
    })
  },
})

// create tag
exports.MutationCreateTag = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('createTag', {
      type: Tag,
      args: {
        title: nonNull(stringArg()),
        color: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, args, context) {

        return await context.prismaClientRestaurant.tag.create({
          data: {
            ...args,
            userId: context.user.id
          },
        })

      },
    })
  },
})

// Update Tag
exports.MutationUpdateTag = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('updateTag', {
      type: Tag,
      args: {
        id: nonNull(stringArg()),
        title: stringArg(),
        color: stringArg(),
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id, ...data }, context) {

        return await context.prismaClientRestaurant.tag.update({
          where: { id },
          data,
        })

      },
    })
  },
})

// Delete Tag
exports.MutationDeleteTag = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('deleteTag', {
      type: Tag,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id }, context) {

        return await context.prismaClientRestaurant.tag.delete({
          where: { id }
        })

      },
    })
  },
})