const {
  nonNull,
  objectType,
  stringArg,
  extendType,
} = require('nexus')

// Import middleware
const isAuth = require('../../middleware/isAuth')

// GraphQL type definition
const Order = objectType({
  name: 'Order',
  definition(t) {
    t.string('id')
    t.string('createdAt')
    t.string('updatedAt')
    t.string('deliveredAt')
    t.string('recipeId')
  },
})

// Exports
exports.Order = Order

// Read Unique Order
exports.QueryOrderByID = extendType({
  type: 'Query',
  definition(t) {
    t.nonNull.field('Order', {
      type: Order,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, args, context) {

        return await context.prismaClientRestaurant.order.findFirst({
          where: {
            id: args.id,
            userId: context.user.id
          },
        })

      },
    })
  },
})

// Create Recipe Command
exports.MutationCreateOrder = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('createOrder', {
      type: Order,
      args: {
        recipeId: nonNull(stringArg()),
        deliveredAt: stringArg()
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, args, context) {

        const ingredientList = await context.prismaClientRestaurant.recipe.findUnique({
          where: {
            id: args?.recipeId,
          },
        })
          .ingredients()


        // Command will be created
        // Then ingredients quantity must be updated (-quantity)
        ingredientList.forEach(async element => {

          await context.prismaClientRestaurant.ingredient.update({
            where: {
              id: element.ingredientId
            },
            data: {
              quantity: {
                increment: - element.quantity,
              }
            }
          })

        })

        return await context.prismaClientRestaurant.order.create({
          data: {
            userId: context.user.id,
            ...args
          },
        })

      },
    })
  },
})

// Update Recipe Command
// exports.MutationUpdateOrder = extendType({
//   type: 'Mutation',
//   definition(t) {
//     t.nonNull.field('updateOrder', {
//       type: Order,
//       args: {
//         id: nonNull(stringArg()),
//         deliveredAt: stringArg()
//       },
//       authorize: (root, args, ctx) => ctx.isAuth,
//       async resolve(_parent, { id, ...data }, context) {

//         const order = await context.prismaClientRestaurant.order.findFirst({
//           where: {
//             id,
//             userId: context.user.id
//           },
//         })

//         if (!order) throw new Error(`No order with id: ${id}`)

//         return await context.prismaClientRestaurant.order.update({
//           where: { id },
//           data,
//         })

//       },
//     })
//   },
// })

// Delete Recipe Command
exports.MutationDeleteOrder = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('deleteOrder', {
      type: Order,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id }, context) {

        const order = await context.prismaClientRestaurant.order.findUnique({
          where: { id }
        })

        const ingredientList = await context.prismaClientRestaurant.recipe.findUnique({
          where: {
            id: order.recipeId,
          },
        })
          .ingredients()

        // Command will be deleted
        // Then ingredients quantity must be updated (+quantity)
        ingredientList.forEach(async element => {

          await context.prismaClientRestaurant.ingredient.update({
            where: {
              id: element.ingredientId
            },
            data: {
              quantity: {
                increment: element.quantity,
              }
            }
          })

        })

        return await context.prismaClientRestaurant.order.delete({
          where: { id }
        })

      },
    })
  },
})