const {
  enumType,
  objectType,
  stringArg,
  nonNull,
  extendType,
} = require('nexus')

// Import functions
const { createJwt } = require('../../utils/jwt')
const {
  doesPasswordsMatch,
  wrongCredentialsMessage
} = require('../../utils/hash')

const User = objectType({
  name: 'User',
  definition(t) {
    t.string('id')
    t.string('createdAt')
    t.string('updatedAt')
    t.string('name')
    t.string('email')
    t.field('role', { type: Role })

    // User's ingredients
    t.list.field('ingredients', {
      type: 'Ingredient',
      async resolve(_parent, _args, context) {
        return await context.prismaClientRestaurant.user
          .findUnique({
            where: {
              id: _parent.id,
            },
          }).ingredients()
      },
    })

    // User's recipes
    t.list.field('recipes', {
      type: 'Recipe',
      async resolve(_parent, _args, context) {
        return await context.prismaClientRestaurant.user
          .findUnique({
            where: {
              id: _parent.id,
            },
          }).recipes()
      },
    })

    // User's recipes
    t.list.field('orders', {
      type: 'Order',
      async resolve(_parent, _args, context) {
        return await context.prismaClientRestaurant.user
          .findUnique({
            where: {
              id: _parent.id,
            },
          })
          .orders()
      },
    })

  },
})

const Role = enumType({
  name: 'Role',
  members: ['USER', 'ADMIN'],
})

const Token = objectType({
  name: 'Token',
  definition(t) {
    t.string('id')
    t.string('createdAt')
    t.string('updatedAt')
    t.boolean('valid')
    t.string('expiration')
    t.string('token')
    t.string('userId')
  },
})

// Exports
exports.User = User
exports.Role = Role
exports.Token = Token

exports.QueryReadUserSelf = extendType({
  type: 'Query',
  definition(t) {
    t.field('readUserSelf', {
      type: 'User',
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_, _args, context) {

        return await context.prismaClientID.user.findUnique({
          where: {
            id: context.user.id
          }
        })
      },
    })
  },
})

exports.MutationCreateUser = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('createUser', {
      type: 'Token',
      args: {
        name: nonNull(stringArg()),
        email: nonNull(stringArg()),
        password: nonNull(stringArg()),
        role: stringArg()
      },
      async resolve(_parent, args, context) {

        const user = await context.prismaClientID.user.create({
          data: args,
        })

        const userRestaurant = await context.prismaClientRestaurant.user.create({
          data: {
            id: user.id
          },
        })

        return await createJwt({ id: user.id }, context.prismaClientID)
      },
    })
  },
})

exports.MutationUserLogin = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('userLogin', {
      type: 'Token',
      args: {
        email: nonNull(stringArg()),
        password: nonNull(stringArg()),
      },
      async resolve(_parent, { email, password }, context) {

        // Verify email
        const user = await context.prismaClientID.user.findUnique({
          where: { email },
        })
        if (!user) throw new Error(wrongCredentialsMessage)

        // Verify password
        const isMatch = await doesPasswordsMatch(password, user.password)
        if (!isMatch) throw new Error(wrongCredentialsMessage)

        return await createJwt({ id: user.id }, context.prismaClientID)
      },
    })
  },
})

exports.MutationUpdateUser = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('updateUser', {
      type: 'User',
      args: {
        name: stringArg(),
        email: stringArg(),
        password: stringArg()
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, args, context) {

        return await context.prismaClientID.user.update({
          where: { id: context.user.id },
          data: args,
        })
      },
    })
  },
})

exports.MutationDeleteUser = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('deleteUser', {
      type: User,
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, args, context) {

        const userRestaurant = await context.prismaClientRestaurant.user.delete({
          where: { id: context.user.id }
        })

        return await context.prismaClientID.user.delete({
          where: { id: context.user.id }
        })

      },
    })
  },
})
