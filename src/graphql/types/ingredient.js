const {
  floatArg,
  nonNull,
  objectType,
  stringArg,
  extendType,
  enumType
} = require('nexus')

const Ingredient = objectType({
  name: 'Ingredient',
  definition(t) {
    t.string('id')
    t.string('createdAt')
    t.string('updatedAt')
    t.string('userId')

    t.string('title')
    t.string('description')
    t.float('quantity')
    t.field('unit', { type: Unit })

    // User's recipes
    t.list.field('recipes', {
      type: 'Recipe',
      async resolve(_parent, _args, context) {
        return await context.prismaClientRestaurant.ingredient
          .findUnique({
            where: {
              id: _parent.id,
            },
          }).recipes()
      },
    })
  },
})

const Unit = enumType({
  name: 'Unit',
  members: ['GRAM', 'LITER', 'SINGULAR'],
})

// const Edge = objectType({
//   name: 'Edges',
//   definition(t) {
//     t.string('cursor')
//     t.field('node', {
//       type: Ingredient,
//     })
//   },
// })

// const PageInfo = objectType({
//   name: 'PageInfo',
//   definition(t) {
//     t.string('endCursor')
//     t.boolean('hasNextPage')
//   },
// })

// const Response = objectType({
//   name: 'Response',
//   definition(t) {
//     t.field('pageInfo', { type: PageInfo })
//     t.list.field('edges', {
//       type: Edge,
//     })
//   },
// })

// Exports
exports.Ingredient = Ingredient
exports.Unit = Unit
// exports.Edge = Edge
// exports.PageInfo = PageInfo
// exports.Response = Response

// Read Unique Ingredient
exports.QueryIngredientByID = extendType({
  type: 'Query',
  definition(t) {
    t.nonNull.field('Ingredient', {
      type: Ingredient,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id }, context) {

        return await context.prismaClientRestaurant.ingredient.findFirst({
          where: {
            id,
            userId: context.user.id
          },
        })

      },
    })
  },
})

// create ingredient
exports.MutationCreateIngredient = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('createIngredient', {
      type: Ingredient,
      args: {
        title: nonNull(stringArg()),
        description: nonNull(stringArg()),
        quantity: floatArg(),
        unit: stringArg(),
        // unit: Unit,
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, args, context) {

        return await context.prismaClientRestaurant.ingredient.create({
          data: {
            ...args,
            userId: context.user.id
          },
        })

      },
    })
  },
})

// Update Ingredient
exports.MutationUpdateIngredient = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('updateIngredient', {
      type: Ingredient,
      args: {
        id: nonNull(stringArg()),
        title: stringArg(),
        description: stringArg(),
        quantity: floatArg(),
        unit: stringArg(),
        // unit: Unit,
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id, ...data }, context) {

        return await context.prismaClientRestaurant.ingredient.update({
          where: { id },
          data,
        })

      },
    })
  },
})

// Delete Ingredient
exports.MutationDeleteIngredient = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('deleteIngredient', {
      type: Ingredient,
      args: {
        id: nonNull(stringArg())
      },
      authorize: (root, args, ctx) => ctx.isAuth,
      async resolve(_parent, { id }, context) {

        return await context.prismaClientRestaurant.ingredient.delete({
          where: { id }
        })

      },
    })
  },
})