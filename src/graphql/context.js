// Import Prisma clients
const {
  // prisma,
  prismaClientID,
  prismaClientXks,
  prismaClientRestaurant
} = require('../lib/prisma')

// Import functions
const { decodeJwt } = require('../utils/jwt')

const context = async function ({ req }) {

  const result = {
    // prisma,
    prismaClientID,
    prismaClientXks,
    prismaClientRestaurant,
    isAuth: false
  }

  try {

    const token = req.header('Authorization').replace('Bearer ', '')
    const decode = await decodeJwt(token)

    const user = await prismaClientID.user.findUnique({
      where: {
        id: decode.id
      }
    })

    return {
      user,
      ...result,
      isAuth: true
    }

  } catch (error) {

    return result
  }
}

module.exports = context