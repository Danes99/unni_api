// Import downloaded modules
const { hash, compare } = require('bcryptjs')

// Import environment variables
const salt = process.env.SALT || 12
const wrongCredentialsMessage = process.env.WRONG_CREDENTIALS_MESSAGE || 'Invalid credentials'

exports.wrongCredentialsMessage = wrongCredentialsMessage

exports.hashPassword = async (password) => (
  await hash(password, salt)
)

exports.doesPasswordsMatch = async (password, hashedPassword) => (
  await compare(password, hashedPassword)
)