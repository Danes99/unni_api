// Import pre-installed modules
const ms = require('ms')

// Import downloaded modules
const { sign, verify } = require('jsonwebtoken')

// Import env variables
const JWT_SECRET = process.env.JWT_SECRET || 'secret'
const JWT_EXPIRES_IN = process.env.JWT_EXPIRES_IN || '12h'

// Custom functions
const generateExpirationDate = () => (
  new Date(Date.now() + ms(JWT_EXPIRES_IN))
)

const generateJwt = async (data) => ({
  token: sign(
    data,
    JWT_SECRET,
    { expiresIn: JWT_EXPIRES_IN }
  ),
  expiration: new Date(Date.now() + ms(JWT_EXPIRES_IN))
})

const createJwt = async (data, prisma) => {

  const token = await generateJwt(data)

  return await prisma.token.create({
    data: {
      ...token,
      userId: data.id
    }
  })
}

const decodeJwt = async (token) => (
  verify(token, JWT_SECRET)
)

module.exports = {
  generateJwt,
  createJwt,
  decodeJwt
}