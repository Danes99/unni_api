// Import downloaded modules
const express = require('express')
const { ApolloServer } = require("apollo-server-express")

// Import GraphQL parameters
const schema = require('./graphql/schema')
const context = require('./graphql/context')

// Express.js server
const app = express()

// App Variables
app.set('AppName', 'GraphQL API')

// Define express config
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

const createApp = async () => {

  // Create Apollo server
  const server = new ApolloServer({
    context,
    schema
  })

  await server.start()
  await server.applyMiddleware({ app })

  return { app, server }

}

module.exports = createApp