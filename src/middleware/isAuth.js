// Import downloaded modules
const { AuthenticationError } = require('apollo-server-express')

const isAuth = ({ user }) => {

  if (!user) {
    throw new AuthenticationError(
      'Authentication token is invalid, please log in',
    )
  }

}

module.exports = isAuth