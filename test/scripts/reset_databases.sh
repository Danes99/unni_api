#!/bin/bash
# Does it have execute permissions? Try a chmod +x scriptname and then ./scriptname
clear

# Remove docker volumes for each databases
# The databases need to be clear/empty for the tests
cd `dirname "$0"`

docker-compose -f postgres_test.yaml down

echo "Removing Docker volume for test database: ID"
docker volume rm unni_postgres_id_test

echo "Removing Docker volume for test database: XKS"
docker volume rm unni_postgres_xks_test

echo "Removing Docker volume for test database: Restaurant"
docker volume rm unni_postgres_restaurant_test

echo "Every Docker volume has been removed"

# docker-compose -f postgres_test.yaml up -d

# Polulate databases
# cd ../../
# npm run prisma-push-local-test

# docker-compose -f postgres_test.yaml down