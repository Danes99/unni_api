// Import build-in modules
const assert = require('assert')

// Import downloaded module
const { expect } = require("chai")
// const graphql = require('graphql')
const { gql } = require('graphql-request')

// Import Express.js app
const createApp = require('../src/app')

// const url = `http://localhost:8082/`;
// const request = require('supertest')(url);
const request = require('supertest')

let app
// let server
let makeRequest

const queryCreateUser = gql`
mutation Mutation($name: String!, $email: String!, $password: String!) {
  createUser(name: $name, email: $email, password: $password) {
    id
    createdAt
    updatedAt
    expiration
    valid
    token
    userId
  }
}`

const queryReadUserSelf = gql`
query ReadUserSelf {
  readUserSelf {
    id
    createdAt
    updatedAt
    name
    email
    role
    orders {
      id
    }
    recipes {
      id
    }
    ingredients {
      id
    }
  }
}`

beforeEach(async () => {

  const result = await createApp()
  app = result.app

  makeRequest = ({ query, variables = {}, token = '' }) => {

    return new Promise((resolve, reject) => {
      request(app)
        .post('/graphql')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({ query, variables })
        .expect(200)
        .end((error, res) => {
          // res will contain array with one user
          if (error) resolve(res.body)
          resolve(res.body)
        })
    })
  }

})

describe('DB: ID', async () => {

  it('Can create a user', async () => {

    const createUserVariables = {
      name: "TestIdentity1",
      email: "TestIdentity1.doe@email.com",
      password: "pass12345"
    }

    const createUserResponse = await makeRequest({ query: queryCreateUser, variables: createUserVariables })
    const token = createUserResponse.data?.createUser

    // Tests types
    expect(typeof token.id).to.equal('string')
    expect(typeof token.createdAt).to.equal('string')
    expect(typeof token.updatedAt).to.equal('string')
    expect(typeof token.expiration).to.equal('string')
    expect(typeof token.valid).to.equal('boolean')
    expect(typeof token.token).to.equal('string')
    expect(typeof token.userId).to.equal('string')

    // Test values
    expect(token.valid).to.equal(true)

    const readUserResponseWithoutToken = await makeRequest({ query: queryReadUserSelf })
    expect(readUserResponseWithoutToken.data?.readUserSelf).to.equal(null)

    const readUserResponse = await makeRequest({ query: queryReadUserSelf, token: token.token })
    const user = readUserResponse.data?.readUserSelf

    // Tests types
    expect(user).to.not.equal(null)
    expect(typeof user.id).to.equal('string')
    expect(typeof user.createdAt).to.equal('string')
    expect(typeof user.updatedAt).to.equal('string')
    expect(typeof user.name).to.equal('string')
    expect(typeof user.email).to.equal('string')
    expect(typeof user.role).to.equal('string')

    expect(user.orders).to.be.an('array')
    expect(user.ingredients).to.be.an('array')
    expect(user.recipes).to.be.an('array')

    // Test values
    expect(user.id).to.equal(token.userId)
    expect(user.name).to.equal(createUserVariables.name)
    expect(user.email).to.equal(createUserVariables.email)
    expect(user.orders.length).to.be.equal(0)
    expect(user.ingredients.length).to.equal(0)
    expect(user.recipes.length).to.equal(0)
  })

})