FROM node:16.11.1-alpine3.11

WORKDIR /usr/src/app

# Copy only package.json and package-lock.json
# for Docker layer optimisation
# COPY package.json package-lock.json /usr/src/app/

COPY . /usr/src/app/

RUN npm ci

RUN npm run prisma-generate

RUN rm -rf /node_modules

# Install stage

RUN npm ci --production

# RUN npm ci --production

EXPOSE 80

# Run stage

CMD ["npm", "start"]