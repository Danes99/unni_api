// Server Variables
const port = process.env.PORT || 80

// Import Express.js app
const createApp = require('./src/app')

const startServer = async () => {

  const { app, server } = await createApp()

  const appName = process.env.APP_NAME || app.get('AppName') || 'GraphQL API'
  const startMessage = `🚀 ${appName} ready at http://localhost${port ? `:${port}` : ''}${server.graphqlPath}`

  // Start server
  app.listen(
    port,
    () => console.log(startMessage)
  )

}

startServer()